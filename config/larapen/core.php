<?php

return [

    /*
     |--------------------------------------------------------------------------
     | The item's ID on CodeCanyon
     |--------------------------------------------------------------------------
     |
     */

    'item_id' => '18776089',

	/*
	 |--------------------------------------------------------------------------
	 | Purchase code checker URL
	 |--------------------------------------------------------------------------
	 |
	 */

    'purchase_code_checker_url' => 'http://api.bedigit.com/envato.php?purchase_code=',

    /*
     |--------------------------------------------------------------------------
     | Default ads picture for SERP
     |--------------------------------------------------------------------------
     |
     */
    
    'picture' => 'default-img.jpg',
    
    /*
     |--------------------------------------------------------------------------
     | Default user profile picture
     |--------------------------------------------------------------------------
     |
     */
    
    'photo' => '',

    /*
     |--------------------------------------------------------------------------
     | Set as default language the browser language
     |--------------------------------------------------------------------------
     |
     */

    'detect_browser_language' => false,

    /*
     |--------------------------------------------------------------------------
     | Optimize your links for SEO (for International website)
     |--------------------------------------------------------------------------
     |
     */

    'multi_countries_website' => false,

	/*
     |--------------------------------------------------------------------------
     | Force links to use the HTTPS protocol
     |--------------------------------------------------------------------------
     |
     */

	'force_https' => false,

];

<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Larapen\Helpers\Arr;
use App\Larapen\Models\Ad;

class AdSentByEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $ad;
    public $mailData;

    /**
     * Create a new message instance.
     *
     * @param Ad $ad
     * @param $mailData
     */
    public function __construct(Ad $ad, $mailData)
    {
        $this->ad = $ad;
        $this->mailData = (is_array($mailData)) ? Arr::toObject($mailData) : $mailData;

        $this->to($this->mailData->recipient_email, $this->mailData->recipient_email);
		$this->replyTo($this->mailData->sender_email, $this->mailData->sender_email);
        $this->subject(trans('mail.New Suggestion', [
            'app_name' => config('settings.app_name'),
            'country_code' => $ad->country_code
        ]));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ad.sent-by-email');
    }
}

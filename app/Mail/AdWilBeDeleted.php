<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Larapen\Models\Ad;

class AdWilBeDeleted extends Mailable
{
    use Queueable, SerializesModels;

    public $ad;
    public $days;

    /**
     * Create a new message instance.
     *
     * @param Ad $ad
     * @param $days
     */
    public function __construct(Ad $ad, $days)
    {
        $this->ad = $ad;
        $this->days = $days;

        $this->to($ad->contact_email, $ad->contact_name);
        $this->subject(trans('mail.Your ad ":title" will be deleted in :days days', [
            'title' => $ad->title,
            'days' => $days
        ]));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ad.will-be-deleted');
    }
}

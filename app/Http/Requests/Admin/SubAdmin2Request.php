<?php


namespace App\Http\Requests\Admin;

use Backpack\CRUD\app\Http\Requests\CrudRequest as BackpackCrudRequest;

class SubAdmin2Request extends BackpackCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
			'code' => 'required|min:2|max:20',
			'name' => 'required|min:2|max:255',
			'asciiname' => 'required|min:2|max:255',
		];
    }
}

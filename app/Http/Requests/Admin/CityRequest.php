<?php


namespace App\Http\Requests\Admin;

use Backpack\CRUD\app\Http\Requests\CrudRequest as BackpackCrudRequest;

class CityRequest extends BackpackCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_code' => 'required|min:2|max:2',
            'name' => 'required|min:2|max:255',
            'asciiname' => 'required|min:2|max:255',
            'latitude' => 'required',
            'longitude' => 'required',
            'subadmin1_code' => 'required',
            'time_zone' => 'required',
        ];
    }
}

<?php

namespace App\Http\Controllers\Ajax;

use App\Larapen\Models\Category;
use App\Http\Controllers\FrontController;
use Illuminate\Http\Request;

class CategoryController extends FrontController
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function getSubCategories(Request $request)
    {
        $language_code = $request->input('language_code');
        $parent_id = $request->input('cat_id');
        
        // Get Sub-categories by category ID
        $sub_categories = Category::where('parent_id', $parent_id)->where('translation_lang', $language_code)->orderBy('lft')->get();
        
        // Select the parent category if his haven't any sub-categories
        if (empty($sub_categories)) {
            $sub_categories = Category::where('id', $parent_id)->where('translation_lang', $language_code)->orderBy('lft')->get();
        }
        
        if ($sub_categories->count() == 0) {
            return response()->json(['error' => ['message' => t("Error. Please select another category."),], 404]);
        }
        
        return response()->json(['data' => $sub_categories], 200, [], JSON_UNESCAPED_UNICODE);
    }
}

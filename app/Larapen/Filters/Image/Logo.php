<?php


namespace App\Larapen\Filters\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Logo implements FilterInterface
{
    /**
     * Size of filter effects
     *
     * @var integer
     */
    private $sizeW = 228;
    private $sizeH = 40;
    
    /**
     * JPEG Quality of filter effects
     *
     * @var integer
     */
    private $quality = 100;
    
    /**
     * Applies filter effects to given image
     *
     * @param Image $image
     * @return Image
     */
    public function applyFilter(Image $image)
    {
        //return $image->fit($this->sizeW, $this->sizeH)->encode('png', $this->quality);
        return $image->resize($this->sizeW, $this->sizeH, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->encode('png', $this->quality);
    }
}

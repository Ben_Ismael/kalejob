<?php


namespace App\Larapen\Filters\Image;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Medium implements FilterInterface
{
    /**
     * Size of filter effects
     *
     * @var integer
     */
    private $sizeW = 320;
    private $sizeH = 240;
    
    /**
     * JPEG Quality of filter effects
     *
     * @var integer
     */
    private $quality = 90;
    
    /**
     * Applies filter effects to given image
     *
     * @param Image $image
     * @return Image
     */
    public function applyFilter(Image $image)
    {
        return $image->fit($this->sizeW, $this->sizeH)->encode('jpg', $this->quality);
    }
}

<?php

namespace App\Larapen\Events;

use App\Events\Event;
use App\Larapen\Models\Ad;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AdWasVisited extends Event
{
    use SerializesModels;
    
    public $ad;
    
    /**
     * Create a new event instance.
     *
     * @param Ad $ad
     */
    public function __construct(Ad $ad)
    {
        $this->ad = $ad;
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

<?php


namespace App\Larapen\Listeners;

use Carbon\Carbon;
use App\Larapen\Events\UserWasLogged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Request as Request;

class UpdateUserLastLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Handle the event.
     *
     * @param  UserWasLogged $event
     * @return void
     */
    public function handle(UserWasLogged $event)
    {
        $event->user->last_login_at = (session('time_zone')) ? Carbon::now(session('time_zone')) : Carbon::now();
        $event->user->save();
    }
}

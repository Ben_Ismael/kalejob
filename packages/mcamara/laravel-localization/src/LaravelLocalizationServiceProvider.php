<?php


namespace Larapen\LaravelLocalization;

use Illuminate\Support\ServiceProvider;

class LaravelLocalizationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path('laravellocalization.php'),
        ], 'config');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['modules.handler', 'modules'];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // $reflection = new \ReflectionClass(get_parent_class($this));
        // $directory = dirname($reflection->getFileName());
        $directory = __DIR__;
        
        $packageConfigFile = $directory . '/config/config.php';
        
        $this->mergeConfigFrom($packageConfigFile, 'laravellocalization');
        
        $this->app['laravellocalization'] = $this->app->share(function () {
            return new LaravelLocalization();
        });
    }
}

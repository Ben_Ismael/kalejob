
@extends('layouts.master')

@section('search')
	@parent
@endsection

@section('content')
	<div class="main-container inner-page">
		<div class="container">
			<div class="section-content">
				<div class="row">

					<h1 class="text-center title-1"><strong>{{ trans('page.Phishing') }}</strong></h1>
					<hr class="center-block small text-hr">

					<div class="col-md-12 page-content">
						<div class="inner-box relative">
							<div class="row">
								<div class="col-sm-12 page-content">
									<div class="text-content text-left">
										{!! trans('page.phishing_content', ['contactUrl' => '#']) !!}
									</div>
								</div>
							</div>
						</div>
					</div>

					@include('layouts.inc.social.horizontal')
				</div>
			</div>
		</div>
	</div>
@endsection

@section('info')
@endsection

@section('javascript')
	@parent
@endsection

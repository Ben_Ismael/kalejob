<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Kalejob | @yield('title')</title>

    <link href="{{asset('images/favicon.144x144.png')}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="{{asset('images/favicon.114x114.png')}}" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="{{asset('images/favicon.72x72.png')}}" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="{{asset('images/favicon.57x57.png')}}" rel="apple-touch-icon" type="image/png">
    <link href="{{asset('images/favicon.png')}}" rel="icon" type="image/png">
    <link href="{{asset('images/favicon.ico')}}" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link href="{{asset('build/assets/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/jscrollpane/style/jquery.jscrollpane.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/ladda/dist/ladda-themeless.min.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/fullcalendar/dist/fullcalendar.min.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/cleanhtmlaudioplayer/src/player.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/cleanhtmlvideoplayer/src/player.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/owl.carousel/dist/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('build/assets/vendors/ionrangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('build/assets/vendors/datatables/media/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('build/assets/vendors/c3/c3.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('build/assets/vendors/chartist/dist/chartist.min.css')}}">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('build/assets/common/css/source/main.css')}}">

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="{{asset('build/assets/vendors/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/tether/dist/js/tether.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/spin.js/spin.js')}}"></script>
    <script src="{{asset('build/assets/vendors/ladda/dist/ladda.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/html5-form-validation/dist/jquery.validation.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/autosize/dist/autosize.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js')}}"></script>
    <script src="{{asset('build/assets/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js')}}"></script>
    <script src="{{asset('build/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/summernote/dist/summernote.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/ionrangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/nestable/jquery.nestable.js')}}"></script>
    <script src="{{asset('build/assets/vendors/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js')}}"></script>
    <script src="{{asset('build/assets/vendors/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('build/assets/vendors/editable-table/mindmup-editabletable.js')}}"></script>
    <script src="{{asset('build/assets/vendors/d3/d3.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/c3/c3.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/peity/jquery.peity.min.js')}}"></script>
    <!-- v1.0.1 -->
    <script src="{{asset('build/assets/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!-- v1.1.1 -->
    <script src="{{asset('build/assets/vendors/gsap/src/minified/TweenMax.min.js')}}"></script>
    <script src="{{asset('build/assets/vendors/hackertyper/hackertyper.js')}}"></script>
    <script src="{{asset('build/assets/vendors/jquery-countTo/jquery.countTo.js')}}"></script>

    <!-- Clean UI Scripts -->
    <script src="{{asset('build/assets/common/js/common.js')}}"></script>
    <script src="{{asset('build/assets/common/js/demo.temp.js')}}"></script>

    
    @yield('style')

    

</head>

<body class="nav-md">
    <!-- main navigation -->
    @include('_partials.menu')
    <!-- /top navigation -->

    <!-- top navigation -->
    @include('_partials.top_menu')
    <!-- /top navigation -->

    <!-- page content -->
    <section class="page-content">
        <div class="page-content-inner"> 
            @yield('content')                
        </div>
    </section>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        @include('_partials.footer')
    </footer>
    <!-- /footer content -->
</div>
</div>

<!--Javascript-->





@yield('scripts')

</body>
</html>

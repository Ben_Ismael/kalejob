@extends('layouts.template')

@section('title')
Tableau de bord
@stop

@section('content')

 <!-- Dashboard -->
    <div class="dashboard-container">
        <div class="row">
            <div class="col-xl-4 col-lg-12">
                <div class="widget widget-one">
                    <div class="widget-header height-300" style="background-image: url({{asset('images/temp/photos/5.jpeg')}})">
                        <h2 class="color-white">
                            Clean. Simple.<br />Responsive
                        </h2>
                    </div>
                    <div class="widget-body clearfix">
                        <div class="s1">
                            <a class="avatar" href="javascript:void(0);">
                                <img src="{{asset('images/temp/avatars/1.jpg')}}" alt="Alternative text to the image">
                            </a>
                            <br />
                            <strong>Artour Scott</strong>
                            <br />
                            @arteezy
                        </div>
                        <div class="s2">
                            <div class="widget-info">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            </div>
                            <span class="label label-primary">html</span>
                            <span class="label label-primary">javascript</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-12">
                <div class="widget widget-three">
                    <div class="example-calendar-block"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- End dashboard -->

</div>


<!-- Page Scripts -->

@section('scripts')
<script>
    $(function() {

        ///////////////////////////////////////////////////////////
        // ADJUSTABLE TEXTAREA
        autosize($('#textarea'));

        ///////////////////////////////////////////////////////////
        // CUSTOM SCROLL
        if (!cleanUI.hasTouch) {
            $('.custom-scroll').each(function() {
                $(this).jScrollPane({
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100
                });
                var api = $(this).data('jsp'),
                        throttleTimeout;
                $(window).bind('resize', function() {
                    if (!throttleTimeout) {
                        throttleTimeout = setTimeout(function() {
                            api.reinitialise();
                            throttleTimeout = null;
                        }, 50);
                    }
                });
            });
        }

        ///////////////////////////////////////////////////////////
        // CALENDAR
        $('.example-calendar-block').fullCalendar({
            //aspectRatio: 2,
            height: 403,
            header: {
                left: 'prev, next',
                center: 'title',
                right: 'month, agendaWeek, agendaDay'
            },
            buttonIcons: {
                prev: 'none fa fa-arrow-left',
                next: 'none fa fa-arrow-right',
                prevYear: 'none fa fa-arrow-left',
                nextYear: 'none fa fa-arrow-right'
            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            viewRender: function(view, element) {
                if (!cleanUI.hasTouch) {
                    $('.fc-scroller').jScrollPane({
                        autoReinitialise: true,
                        autoReinitialiseDelay: 100
                    });
                }
            },
            defaultDate: '2016-05-12',
            events: [
                {
                    title: 'All Day Event',
                    start: '2016-05-01',
                    className: 'fc-event-success'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-05-09T16:00:00',
                    className: 'fc-event-default'
                },
                {
                    id: 999,
                    title: 'Repeating Event',
                    start: '2016-05-16T16:00:00',
                    className: 'fc-event-success'
                },
                {
                    title: 'Conference',
                    start: '2016-05-11',
                    end: '2016-05-14',
                    className: 'fc-event-danger'
                }
            ],
            eventClick: function(calEvent, jsEvent, view) {
                if (!$(this).hasClass('event-clicked')) {
                    $('.fc-event').removeClass('event-clicked');
                    $(this).addClass('event-clicked');
                }
            }
        });

        ///////////////////////////////////////////////////////////
        // GRAPH
        var cssAnimationData = {
                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    series: [
                        [1, 2, 2.7, 0, 3, 5, 3, 4, 8, 10, 12, 7],
                        [0, 1.2, 2, 7, 2.5, 9, 5, 8, 9, 11, 14, 4],
                        [10, 9, 8, 6.5, 6.8, 6, 5.4, 5.3, 4.5, 4.4, 3, 2.8]
                    ]
                },
                cssAnimationResponsiveOptions = [
                    [{
                        axisX: {
                            labelInterpolationFnc: function(value, index) {
                                return index % 2 !== 0 ? !1 : value
                            }
                        }
                    }]
                ];

        new Chartist.Line(".chartist-one", cssAnimationData, {
                plugins: [
            Chartist.plugins.tooltip()
        ]}, cssAnimationResponsiveOptions);

        ///////////////////////////////////////////////////////////
        // CAROUSEL WIDGET
        $('.carousel-widget').carousel({
            interval: 4000
        });

        $('.carousel-widget-2').carousel({
            interval: 6000
        });

        ///////////////////////////////////////////////////////////
        // TOOLTIPS
        $("[data-toggle=tooltip]").tooltip();

    });
</script>
@stop

<!-- End Page Scripts -->


@stop
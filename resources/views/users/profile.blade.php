@extends('layouts.template')

@section('title')
Profile
@stop

@section('content')



<!-- Profile Header -->
<nav class="top-submenu top-submenu-with-background">
	<div class="profile-header">
		<div class="profile-header-info">
			<div class="row">
				<div class="col-xl-8 col-xl-offset-4">
					<div class="width-100 text-center pull-right hidden-md-down">
						<h2>17</h2>
						<p>Posts</p>
					</div>
					<div class="profile-header-title">
						<h2>{{Auth::user()->name}} <small></small></h2>
						<p>Founder / CEO</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</nav>
<!-- End Profile Header -->

<!-- Profile -->
<div class="row">
	<div class="col-xl-4">
		<section class="panel profile-user" style="background-image: url({{ asset('images/temp/photos/6.jpeg') }})">
			<div class="panel-body">
				<div class="profile-user-title text-center">
					<a class="avatar" href="javascript:void(0);">
						<img src="{{ asset('images/temp/avatars/1.jpg') }}" alt="profile-img">
					</a>
					<br />
					<div class="btn-group btn-group-justified" aria-label="" role="group">
						<div class="btn-group">
							<button type="button" class="btn width-150 swal-btn-success">CV</button>
						</div>
						<div class="btn-group">
							<button type="button" class="btn width-150 swal-btn-success-2">Professionnel</button>
						</div>
					</div>
					<br />
					<p>Dernière activité: 13 Mai 2017 7:26</p>
					<p>
						<span class="donut donut-success"></span>
						Online
					</p>
				</div>
			</div>
		</section>
		<section class="panel">
			<div class="panel-body">
				<h6>Actions</h6>
				<div class="btn-group-vertical btn-group-justified">
					<button type="button" class="btn">Modifier CV</button>
					<button type="button" class="btn">Lettre de motivation</button>
					<button type="button" class="btn">Historique des posts</button>
					<button type="button" class="btn">SMS <span class="label label-info font-size-14 margin-inline">74</span></button>
				</div>
			</div>
		</section>

		<section class="panel">
			<div class="panel-body">
				<h6>Vos Expériences</h6>
				<dl class="dl-horizontal user-profile-dl">
					<dt>Diplome</dt>
					<dd>Ingénieur</dd>
					<dt>Address</dt>
					<dd>Abidjan, CI</dd>
					<dt>Compétence</dt>
					<dd><span class="label label-default">html</span> <span class="label label-default">css</span> <span class="label label-default">design</span> <span class="label label-default">javascript</span></dd>
					<dt>Dernière Entreprise</dt>
					<dd>Microsoft, Soft Mailstorm</dd>
					<dt>Votre Description</dt>
					<dd style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</dd>
				</dl>
			</div>
		</section>

	</div>
	<div class="col-xl-8">
		<section class="panel profile-user-content">
			<div class="panel-body">
				<div class="nav-tabs-horizontal">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" href="javascript: void(0);" data-toggle="tab" data-target="#posts" role="tab">
								<i class="icmn-bullhorn"></i>
								Annonces
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="javascript: void(0);" data-toggle="tab" data-target="#messaging" role="tab">
								<i class="icmn-megaphone"></i>
								Offres Spécifiques
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="javascript: void(0);" data-toggle="tab" data-target="#settings" role="tab">
								<i class="icmn-cog"></i>
								Reglages
							</a>
						</li>
					</ul>
					<div class="tab-content padding-vertical-20">
						<div class="tab-pane active" id="posts" role="tabpanel">
							<div class="user-wall">
								<div class="user-wall-posting">
									<div class="form-group padding-top-10 margin-bottom-0">
										<textarea class="form-control adjustable-textarea" placeholder="Votre annonce ici"></textarea>
										<button class="btn btn-primary width-200 margin-top-10">
											<i class="fa fa-send margin-right-5"></i>
											Publiez une offre
										</button>
										<button class="btn btn-link margin-top-10">
											Pièce jointe
										</button>
									</div>
								</div>
								<div class="user-wall-item clearfix">
									<div class="s1">
										<a class="avatar" href="javascript:void(0);">
											<img src="{{asset('images/temp/avatars/3.jpg')}}" alt="Alternative text to the image" />
										</a>
									</div>
									<div class="s2">
										<div class="user-wall-item-head">
											<div class="dropdown pull-right">
												<a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
													<i class="dropdown-icon icmn-list"></i>
													
												</a>
												<ul class="dropdown-menu" aria-labelledby="" role="menu">
													<a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon icmn-pencil7"></i> Edit Post</a>
													<a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon icmn-bin3"></i> Delete Post</a>
													<div class="dropdown-divider"></div>
													<a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon fa fa-recycle"></i> Mark as Spam</a>
												</ul>
											</div>
											<strong>Donald Trump</strong>
											<small>Five seconds ago</small>
										</div>
										<div class="user-wall-post">
											<img class="messaging-img" src="../assets/common/img/temp/photos/4.jpeg" />
											<img class="messaging-img" src="../assets/common/img/temp/photos/3.jpeg" />
											<img class="messaging-img" src="../assets/common/img/temp/photos/2.jpeg" />
											<p>
												Lorem Ipsum is simply dummy text of the printing and typesetting industry.
												<br />
												Ipsum is simply dummy lorem
											</p>
										</div>
										<div class="user-wall-controls">
											<a href="javascript: void(0);" class="margin-right-10">
												<i class="icmn-heart3"></i>
												26 Likes
											</a>
											<a href="javascript: void(0);">
												<i class="icmn-bubble"></i>
												17 Comments
											</a>
										</div>


									</div>
								</div>

								<div class="user-wall-item clearfix">

								</div>

							</div>
						</div>

						<!-- Offres Spécifiques !-->

						<div class="tab-pane" id="messaging" role="tabpanel">

							<!-- Default Panel -->
							<section class="panel">
								<div class="panel-heading">
									<div class="pull-right">
										<span class="margin-right-20">
											User:
											<a href="javascript: void(0);">Administrator</a>
										</span>
										<div class="dropdown">
											<a href="javascript: void(0);" class="dropdown-toggle dropdown-inline-button" data-toggle="dropdown" aria-expanded="false">
												<i class="dropdown-inline-button-icon icmn-list"></i>
												<span class="hidden-lg-down"></span>
												
											</a>
											<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
												<div class="dropdown-header">Active</div>
												<a class="dropdown-item" href="javascript:void(0)">Project Management</a>
												<a class="dropdown-item" href="javascript:void(0)">User Inetrface Development</a>
												<a class="dropdown-item" href="javascript:void(0)">Documentation</a>
												<div class="dropdown-header">Inactive</div>
												<a class="dropdown-item" href="javascript:void(0)">Marketing</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon icmn-cog"></i> Settings</a>
											</ul>
										</div>
									</div>
									<h3>Auditeur Banque</h3>
								</div>
								<div class="panel-body">
									<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam officia, molestiae possimus ab facere labore id sunt quae asperiores, minus minima doloribus laboriosam, modi facilis non et dicta ratione voluptas.</span>
								</div>
								<div class="panel-footer">
									<span><strong> Publier le : 17/04/2014</strong></span>
									<a type="button" class="btn btn-info btn-outline pull-right">Postuler</a>
								</div>
							</section>
							<!-- End Default Panel -->

							<!-- Default Panel -->
							<section class="panel">
								<div class="panel-heading">
									<div class="pull-right">
										<span class="margin-right-20">
											User:
											<a href="javascript: void(0);">Koffi</a>
										</span>
										<div class="dropdown">
											<a href="javascript: void(0);" class="dropdown-toggle dropdown-inline-button" data-toggle="dropdown" aria-expanded="false">
												<i class="dropdown-inline-button-icon icmn-list"></i>
												<span class="hidden-lg-down"></span>
												
											</a>
											<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
												<div class="dropdown-header">Active</div>
												<a class="dropdown-item" href="javascript:void(0)">Project Management</a>
												<a class="dropdown-item" href="javascript:void(0)">User Inetrface Development</a>
												<a class="dropdown-item" href="javascript:void(0)">Documentation</a>
												<div class="dropdown-header">Inactive</div>
												<a class="dropdown-item" href="javascript:void(0)">Marketing</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="javascript:void(0)"><i class="dropdown-icon icmn-cog"></i> Settings</a>
											</ul>
										</div>
									</div>
									<h3>Auditeur Finance</h3>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-md-2">
										
												<a class="avatar" href="javascript:void(0);" style="height: 80px; width: 80px;">
													<img src="{{asset('images/temp/avatars/1.jpg')}}" alt="Alternative text to the image">
												</a>
											
										</div>
										<div class="col-md-9 text-justify">
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam officia, molestiae possimus ab facere labore id sunt quae asperiores, minus minima doloribus laboriosam, modi facilis non et dicta ratione voluptas.</span>
										</div>
									</div>

									
								</div>
								<div class="panel-footer">
									<span><strong> Publier le : 17/04/2014</strong></span>
									<a type="button" class="btn btn-info btn-outline pull-right">Postuler</a>
								</div>
							</section>
							<!-- End Default Panel -->
						</div>

						<!-- Setting tab !-->
						<div class="tab-pane" id="settings" role="tabpanel">
							<br />
							<h5>Personal Information</h5>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="l0">Username</label>
										<input type="text" class="form-control" id="l0">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="l1">Email</label>
										<input type="email" class="form-control" id="l1">
									</div>
								</div>
							</div>
							<br />
							<h5>Password</h5>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="l3">Password</label>
										<input type="password" class="form-control" id="l3">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="form-control-label" for="l4">Password</label>
										<input type="password" class="form-control" id="l4">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<br />
									<h5>Profile Avatar</h5>
									<div class="form-group">
										<label class="form-control-label" for="l6">File Upload</label>
										<input type="file" id="l6">
									</div>
								</div>
								<div class="col-lg-6">
									<br />
									<h5>Profile Background</h5>
									<div class="form-group">
										<label class="form-control-label" for="l5">File Upload</label>
										<input type="file" id="l5">
									</div>
								</div>
							</div>
							<div class="form-actions">
								<div class="form-group">
									<button type="button" class="btn width-150 btn-primary">Submit</button>
									<button type="button" class="btn btn-default">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<!-- End Profile -->


@stop

@section('scripts')

<script>
	$(function() {

        ///////////////////////////////////////////////////////////
        // ADJUSTABLE TEXTAREA
        autosize($('.adjustable-textarea'));

        ///////////////////////////////////////////////////////////
        // MEDIA PLAYER
        if (!(typeof angular === 'undefined')) {
        	$('.mediatec-cleanaudioplayer').cleanaudioplayer();
        	$('.mediatec-cleanvideoplayer').cleanvideoplayer();
        }

        ///////////////////////////////////////////////////////////
        // CALENDAR
        $('.example-calendar-block').fullCalendar({
            //aspectRatio: 2,
            height: 450,
            header: {
            	left: 'prev, next',
            	center: 'title',
            	right: 'month, agendaWeek, agendaDay'
            },
            buttonIcons: {
            	prev: 'none fa fa-arrow-left',
            	next: 'none fa fa-arrow-right',
            	prevYear: 'none fa fa-arrow-left',
            	nextYear: 'none fa fa-arrow-right'
            },
            Actionable: true,
            eventLimit: true, // allow "more" link when too many events
            viewRender: function(view, element) {
            	if (!cleanUI.hasTouch) {
            		$('.fc-scroller').jScrollPane({
            			autoReinitialise: true,
            			autoReinitialiseDelay: 100
            		});
            	}
            },
            eventClick: function(calEvent, jsEvent, view) {
            	if (!$(this).hasClass('event-clicked')) {
            		$('.fc-event').removeClass('event-clicked');
            		$(this).addClass('event-clicked');
            	}
            },
            defaultDate: '2016-05-12',
            events: [
            {
            	title: 'All Day Event',
            	start: '2016-05-01',
            	className: 'fc-event-success'
            },
            {
            	id: 999,
            	title: 'Repeating Event',
            	start: '2016-05-09T16:00:00',
            	className: 'fc-event-default'
            },
            {
            	id: 999,
            	title: 'Repeating Event',
            	start: '2016-05-16T16:00:00',
            	className: 'fc-event-success'
            },
            {
            	title: 'Conference',
            	start: '2016-05-11',
            	end: '2016-05-14',
            	className: 'fc-event-danger'
            }
            ]
        });

        ///////////////////////////////////////////////////////////
        // SWAL ALERTS
        $('.swal-btn-success').click(function(e){
        	e.preventDefault();
        	swal({
        		title: "Following",
        		text: "Now you are following Artour Scott",
        		type: "success",
        		confirmButtonClass: "btn-success",
        		confirmButtonText: "Ok"
        	});
        });

        $('.swal-btn-success-2').click(function(e){
        	e.preventDefault();
        	swal({
        		title: "Friends request",
        		text: "Friends request was succesfully sent to Artour Scott",
        		type: "success",
        		confirmButtonClass: "btn-success",
        		confirmButtonText: "Ok"
        	});
        });

    });
</script>

@stop

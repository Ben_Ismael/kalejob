@extends('layouts.template')

@section('title')
Documents
@stop

@section('content')
 <!-- Mail -->
    <section class="panel panel-with-sidebar sidebar-large panel-with-borders messaging mail">
        <div class="panel-sidebar">
            <div class="messaging-list">
                <div class="messaging-list-item current">
                    <div class="s2">
                        <div class="messaging-list-title-name"><strong>Nombre de CV : 4</strong></div>
                        <div>
                            <small class="messaging-time"></small>
                            <div class="messaging-list-title-subject">Libellé dernier CV : CV modifier 2</div>
                           
                        </div>
                    </div>
                </div>

                <div class="messaging-list-item">
                    <div class="s2">
                        <div class="messaging-list-title-name"><strong>Nombre de CV : 4</strong></div>
                        <div>
                            <small class="messaging-time"></small>
                            <div class="messaging-list-title-subject">Libellé dernier CV : CV modifier 2</div>
                           
                        </div>
                    </div>
                </div>
              
                
                <div class="messaging-list-item">
                    <div class="s2">
                        <div class="messaging-list-title-name"><strong>Deleted Messages</strong></div>
                        <div>
                            <small class="messaging-time">9:59PM</small>
                            <div class="messaging-list-title-subject">Little Question</div>
                            <div class="messaging-list-title-last color-default">Hello! Where you are now? I want to talk</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-heading">
            <div class="mail-heading">
                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="javascript: void(0);" data-toggle="tab" data-target="#profile2" role="tab" aria-expanded="false">
                                <i class="icmn-home"></i>
                                <span class="hidden-md-down">CV</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" data-toggle="tab" data-target="#messages2" role="tab">
                                <i class="icmn-users"></i>
                                <span class="hidden-md-down">Lettres de motivations</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript: void(0);" data-toggle="tab" data-target="#settings2" role="tab">
                                <i class="icmn-price-tags"></i>
                                <span class="hidden-md-down">Autres</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="mail-heading-buttons pull-right">
                <a href="javascrit: void(0);" class="btn btn-primary" data-toggle="modal" data-target="#compose">Compose</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="margin-bottom-0">
                        <table class="table table-hover nowrap" id="example1" width="100%">
                            <thead>
                                <tr>
                                    <th class="no-sort width-10"></th>
                                    <th class="no-sort width-10"></th>
                                    <th class="width-150">From</th>
                                    <th>Message</th>
                                    <th class="no-sort width-10"></th>
                                    <th class="no-sort width-50"></th>
                                </tr>
                            </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>From</th>
                                        <th>Message</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-warning"></i></td>
                                    <td>GoDaddy</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>2:54AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Hetzner</td>
                                    <td>Ipsum is simply dummy text of the printing and typesetting</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>6:39AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>mail@google.com</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing</td>
                                    <td></td>
                                    <td>1:48AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Google.com</td>
                                    <td>Is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>8:38AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-warning"></i></td>
                                    <td>GoDaddy</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>2:54AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Hetzner</td>
                                    <td>Ipsum is simply dummy text of the printing and typesetting</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>6:39AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>mail@google.com</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing</td>
                                    <td></td>
                                    <td>1:48AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Google.com</td>
                                    <td>Is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>8:38AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-warning"></i></td>
                                    <td>GoDaddy</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>2:54AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Hetzner</td>
                                    <td>Ipsum is simply dummy text of the printing and typesetting</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>6:39AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>mail@google.com</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing</td>
                                    <td></td>
                                    <td>1:48AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Google.com</td>
                                    <td>Is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>8:38AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-warning"></i></td>
                                    <td>GoDaddy</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>2:54AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Hetzner</td>
                                    <td>Ipsum is simply dummy text of the printing and typesetting</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>6:39AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>mail@google.com</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing</td>
                                    <td></td>
                                    <td>1:48AM</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><input type="checkbox"></td>
                                    <td><i class="icmn-star-full color-default opacity-03"></i></td>
                                    <td>Google.com</td>
                                    <td>Is simply dummy text of the printing and typesetting industry</td>
                                    <td><i class="icmn-attachment color-default"></i></td>
                                    <td>8:38AM</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Mail -->

    <!-- Compose Modal -->
    <div class="modal fade modal-size-large" id="compose" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Compose Message</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="To" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Subject" />
                    </div>
                    <div class="compose-message">
                        <div class="summernote"></div>
                    </div>
                </div>
                <div class="modal-footer text-left">
                    <button type="button" class="btn width-150 btn-primary"><i class="fa fa-send margin-right-5"></i> Send</button>
                    <button type="button" class="btn btn-link">Attachment</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Compose Modal -->

@stop

@section('scripts')

<script>
    $(function() {

        ///////////////////////////////////////////////////
        // SIDEBAR CURRENT STATE
        $('.messaging-list-item').on('click', function(){
            $('.messaging-list-item').removeClass('current');
            $(this).addClass('current');
        });

        ///////////////////////////////////////////////////
        // TEXT EDITOR
        $(function() {
            $('.summernote').summernote({
                height: 200
            });
        });

        ///////////////////////////////////////////////////
        // DATATABLES
        $('#example1').DataTable({
            responsive: true,
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false
            }]
        });

    });
</script>

@stop
<nav class="left-menu" left-menu>
    <div class="logo-container">
        <a href="{{route('users.index')}}" class="logo">
            <img src="{{asset('images/logo.png')}}" alt="Clean UI Admin Template" />
            <img class="logo-inverse" src="{{asset('images/logo-inverse.png')}}" alt="Clean UI Admin Template" />
        </a>
    </div>
    <div class="left-menu-inner scroll-pane">
        <ul class="left-menu-list left-menu-list-root list-unstyled">
            
            <li class="left-menu-list-active">
                <a class="left-menu-link" href="{{route('users.index')}}">
                    <i class="left-menu-link-icon icmn-home2"><!-- --></i>
                    <span class="menu-top-hidden">Tableau de bord</span>
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="{{route('users.profile')}}">
                    <i class="left-menu-link-icon icmn-profile"><!-- --></i>
                    Profile
                </a>
            </li>
            <li class="left-menu-list-submenu">
                <a class="left-menu-link" href="javascript: void(0);">
                <i class="left-menu-link-icon icmn-cog2"><!-- --></i>
                    Outils de membres
                </a>
                <ul class="left-menu-list list-unstyled">
                    <li>
                        <a class="left-menu-link" href="forms-dropdowns.html">
                            Chat
                        </a>
                    </li>
                </ul>
            </li>
            
            <li>
                <a class="left-menu-link" href="{{route('users.document')}}">
                    <i class="left-menu-link-icon icmn-archive"><!-- --></i>
                    Documents
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="apps-messaging.html">
                    <i class="left-menu-link-icon icmn-bullhorn"><!-- --></i>
                    Annonces
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="apps-mail.html">
                    <i class="left-menu-link-icon icmn-phone"><!-- --></i>
                    Alerte SMS <span class="label label-danger font-size-14 margin-inline">Non-actif</span>
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="apps-gallery.html">
                    <i class="left-menu-link-icon icmn-tv"><!-- --></i>
                    KalejobTV <span class="label label-warning font-size-14 margin-inline">Offre vidéo</span>
                </a>
            </li>
            <li class="left-menu-list-separator"><!-- --></li>
                 <li class="menu-top-hidden no-colorful-menu">
                   <div class="left-menu-item">
                     Espace Pub
                </div>    
            </li>
            <li class="left-menu-list-separator"><!-- --></li>
            
            <li class="menu-top-hidden">
                <div class="left-menu-item">
                    <span class="donut donut-success"></span> #Pays
                </div>
            </li>
            
        </ul>
    </div>
</nav>